<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
        $result = $this->db->get('students');
		$data = array('consulta' => $result);
		$this->load->model("post");
		$data["getPostByName"] = $this->post->getPostByName();
		$this->load->view('home',$data);
	}
}
