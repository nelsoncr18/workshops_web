
<?php
  require 'database.php';
  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <title>Document</title>
</head>
<body>
<div class="container">
    <div class="msg">
      <?php echo $message; ?>
    </div>
    <h1>Form Registration</h1>
    <form action="index.php" method="POST" class="form-inline" role="form">
      <div class="form-group">
        <label class="sr-only" for="">Id</label>
        <input type="text" class="form-control" id="" name="id" placeholder="Your id">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Nombre</label>
        <input type="text" class="form-control" id="" name="nombre" placeholder="Your name">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Categorias</label>
        <input type="text" class="form-control" id="" name="categorias" placeholder="categorias">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<div class="container">
    <table class="table table-striped table-bordered">
        <tr>
            <th>id</th>
            <th>Nombre</th>
            <th>Categorias</th>
            <th></th>
        </tr>
        <?php
          $consultar = "SELECT * FROM categorias";
          $connection = mysqli_connect('localhost:3306', 'root', '', 'categorias');
          $query = mysqli_query($connection, $consultar);
          foreach($query as $row){?>
            <tr>
              <td><?php echo $row['id'];?></td>
              <td><?php echo $row['nombre'];?></td>
              <td><?php echo $row['categorias'];?></td>
              <td><a class="btn btn-danger delete-btn" href="row.php?id=<?php echo $row['id']?>" value="btnEliminar">Eliminar</a></td>
            </tr>
            
            <?php
            }
          ?>
    </table>
</div>


</body>
</html>

